﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour {

    enum BoulderState
    {
        Idle,
        Thrown
    }

    // Trail for the boulder.
    private TrailRenderer _trail;    
    private Transform _parentSling;
    // Position, scale and rotation of the boulder when its on the sling.
    private Vector3 _onSlingPosition;
    private Quaternion _onSlingRotation;
    private Vector3 _onSlingScale;
    // Holds the current state of the boulder.
    private BoulderState _boulderState;
    // Hold the time at which the boulder was last throwm
    private float timeSinceThrown;

    void Start()
    {
        _boulderState = BoulderState.Idle;        
        _onSlingPosition = transform.position;
        _onSlingRotation = transform.rotation;
        _onSlingScale = transform.localScale;
        _parentSling = transform.parent;
        _trail = GetComponent<TrailRenderer>();
    }

    void FixedUpdate()
    {
        // Reset the scene if after max time and boulder has reached or gone below min velocity.
        if (_boulderState == BoulderState.Thrown && Time.time - timeSinceThrown > Constants.RESET_TIME && GetComponent<Rigidbody>().velocity.sqrMagnitude < Constants.MIN_VELOCITY)
        {
            _boulderState = BoulderState.Idle;
            GameManager.Instance.ResetCatapult();
        }
    }

    public void OnThrow()
    {
        if (_boulderState == BoulderState.Idle)
        {
            timeSinceThrown = Time.time;
            // Un-parenting the boulder from the sling and turning of isKinematic to let physics control it.
            transform.parent = null;
            GetComponent<Rigidbody>().isKinematic = false;
            // Enabling the trail if it exists.
            if (_trail)            
                _trail.enabled = true;
                        
            _boulderState = BoulderState.Thrown;            
        }
    }

    public void ResetBoulder()
    {
        _boulderState = BoulderState.Idle;
        // Disabling collider so that the object doesn't hit anything while resetting.
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        transform.parent = _parentSling;
        transform.localScale = _onSlingScale;
        transform.position = _onSlingPosition;
        transform.rotation = _onSlingRotation;
        GetComponent<SphereCollider>().enabled = true;
        if (_trail)
            _trail.enabled = false;
    }
}
