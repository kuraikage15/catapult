﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catapult : MonoBehaviour {

    enum CatapultState
    {
        Idle,
        UserTargeting,
        BoulderThrown
    }

    // Origin transforms of the sling strings.
    public Transform slingLeftAnchorPoint, slingRightAnchorPoint;
    // Origin transforms of the sling strings.
    public Transform slingLeftStringOrigin, slingRightStringOrigin;
    // Line renderers to simulate the strings of the sling.
    public LineRenderer catapultLeftLineRenderer, catapultRightLineRenderer;
    // Line renderer to show the direction of the shot
    public LineRenderer shotDirectionIndicator;

    [Space]
    // Catapult Tranform
    public Transform catapultTransform;
    // Sling's spring joint transform.
    public Transform slingSpringJointTranform;
    // Sling's spring joint connected body transform
    public Transform slingSpringJointBodyTranform;

    [Space]
    // Minimum and Maximum pull distance allowed in the Z direction.
    public float minPullDistanceZ;        
    public float maxPullDistanceZ;
    // Minimum pull distance required to be considered as throw activation.
    public float minPullActivationDistanceZ;
    // Minimum and Maximum pull distance allowed in the X direction.
    public float minPullDistanceX;
    public float maxPullDistanceX;
    
    private CatapultState _catapultState;
    private Vector3 _slingIdlePosition;
    private Quaternion _slingIdleRotation;

    // Use this for initialization.
    void Start () {

        _catapultState = CatapultState.Idle;
        _slingIdlePosition = slingSpringJointTranform.position;
        _slingIdleRotation = slingSpringJointTranform.rotation;

        // Setting the offset of mins and maxes based on the catapult position.
        minPullDistanceZ += catapultTransform.position.z;
        maxPullDistanceZ += catapultTransform.position.z;
        minPullActivationDistanceZ += catapultTransform.position.z;
        minPullDistanceX += catapultTransform.position.x;
        maxPullDistanceX += catapultTransform.position.x;

        // Draw the lines from the sling body to the anchor point to simulate strings.
        if (catapultLeftLineRenderer)
        {
            catapultLeftLineRenderer.SetPosition(0, slingLeftAnchorPoint.position);
            catapultLeftLineRenderer.SetPosition(1, slingLeftStringOrigin.position);            
        }
        if (catapultRightLineRenderer)
        {
            catapultRightLineRenderer.SetPosition(0, slingRightAnchorPoint.position);
            catapultRightLineRenderer.SetPosition(1, slingRightStringOrigin.position);
        }

        // Draw line from spring joint to spring body to show the direction of the shot
        if (shotDirectionIndicator)
        {
            shotDirectionIndicator.SetPosition(0, slingSpringJointBodyTranform.position + new Vector3(0, 0.5f, 0));
            shotDirectionIndicator.SetPosition(1, slingSpringJointTranform.position + new Vector3(0, 0.5f, 0));
        }


    }
	
	// Update is called once per frame.
	void Update () {        

        switch (_catapultState)
        {
            case CatapultState.Idle:
                if (Input.GetMouseButtonDown(0))
                {
                    _catapultState = CatapultState.UserTargeting;
                }
                break;
            case CatapultState.UserTargeting:
                if (Input.GetMouseButton(0))
                {
                    // Updating the line renderers to match up with the movement of the sling/catapult.
                    UpdateSlingLineRenderers();
                    Vector3 touchScreenPoint = Input.mousePosition;
                    touchScreenPoint.z = 20;
                    Vector3 touchWorldPoint = Camera.main.ScreenToWorldPoint(touchScreenPoint);
                    // We take Y of screen point as the Z direction pull of the sling and X as X itself.
                    touchWorldPoint.y = Mathf.Clamp(touchWorldPoint.y, maxPullDistanceZ, minPullDistanceZ);
                    touchWorldPoint.x = Mathf.Clamp(touchWorldPoint.x, minPullDistanceX, maxPullDistanceX);
                    slingSpringJointTranform.position = new Vector3(touchWorldPoint.x, 0, touchWorldPoint.y);
                }
                else
                {
                    if (slingSpringJointTranform.position.z < minPullActivationDistanceZ)
                    {
                        // Updating the line renderers to match up with the movement of the sling/catapult
                        UpdateSlingLineRenderers();
                        ThrowBoulder();
                        _catapultState = CatapultState.BoulderThrown;
                    }
                    else
                    {
                        ResetSling();
                    }
                }
                break;
            case CatapultState.BoulderThrown:
                // Updating the line renderers to match up with the movement of the sling.
                UpdateSlingLineRenderers();
                break;
        }
    }

    // Throws the boulder in the set direction
    public void ThrowBoulder()
    {        
        // Turning off isKinematic on the spring joint for it to move and be controlled by physics.
        slingSpringJointTranform.GetComponent<Rigidbody>().isKinematic = false;
        GameManager.Instance.OnThrow();               
    }

    // Updates sling's line renderers.
    void UpdateSlingLineRenderers()
    {
        if(shotDirectionIndicator)
            shotDirectionIndicator.SetPosition(1, slingSpringJointTranform.position + new Vector3(0, 0.5f, 0));
        if (catapultLeftLineRenderer)
            catapultLeftLineRenderer.SetPosition(1, slingLeftStringOrigin.position);
        if (catapultRightLineRenderer)
            catapultRightLineRenderer.SetPosition(1, slingRightStringOrigin.position);
    }

    // Resets the sling back to the initial state.
    public void ResetSling()
    {
        _catapultState = CatapultState.Idle;
        slingSpringJointTranform.GetComponent<Rigidbody>().isKinematic = true;
        slingSpringJointTranform.position = _slingIdlePosition;
        slingSpringJointTranform.rotation = _slingIdleRotation;
        UpdateSlingLineRenderers();
    }
}
