﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public CameraControl cameraControl;
    public Catapult catapult;
    public GameObject boulderPrefab;
    
    private GameObject _boulder;

    void Awake()
    {
        if (!Instance)
            Instance = this;
    }

	// Use this for initialization.
	void Start () {
        // Generating and setting the boulder on the sling.
        if (boulderPrefab)
            _boulder = Instantiate(boulderPrefab, catapult.slingSpringJointTranform);
    }
		

    public void OnThrow()
    {                
        _boulder.GetComponent<Boulder>().OnThrow();
        // Switching camera to follow the boulder.
        cameraControl.GetComponent<CameraControl>().OnThrow(_boulder.transform);        
    }

    public void ResetCatapult()
    {
        catapult.ResetSling();
        cameraControl.ResetCamera();
        _boulder.GetComponent<Boulder>().ResetBoulder();
    }

    public void Reload()
    {        
        SceneManager.LoadScene(0);
    }
}
