﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants {

    public const float MIN_VELOCITY = 0.5f;
    public const float RESET_TIME = 4.0f;
}
