﻿using Cinemachine;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public CinemachineVirtualCamera virtualCamIdle;
    public CinemachineVirtualCamera virtualCamBoulderFollow;    

    public void OnThrow(Transform boulder)
    {
        virtualCamBoulderFollow.LookAt = boulder;
        virtualCamBoulderFollow.Follow = boulder;
        virtualCamIdle.gameObject.SetActive(false);
        virtualCamBoulderFollow.gameObject.SetActive(true);
    }

    public void ResetCamera()
    {
        virtualCamBoulderFollow.gameObject.SetActive(false);
        virtualCamIdle.gameObject.SetActive(true);
    }
}
